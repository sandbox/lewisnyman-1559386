README.txt
==========

Everybody loves the overlay! The epitome of grace, modestly, and reliance. 

Obnoxious overlay gives the overlay a sense of awareness and confidence. Using modern HTML5 animation techniques, the overlay no longer wanders but strides* confidently on to your screen. Impress your co-workers with an admin interface that's the life of the party. Delight your clients years from now once they upgrade to a real browser!

*Or bounces, spins, fades or jumps. Your milage may vary.